----------------------------------------------------------------
------------------------------------------------------------------------
--                 Xmonad's Javier Pacheco settings                   --
------------------------------------------------------------------------
------------------------------------------------------------------------


------------------------------------------------------------------------
------------------------------------------------------------------------
--                              import                                --
------------------------------------------------------------------------
------------------------------------------------------------------------

import XMonad hiding ( (|||) ) -- jump to layout
import XMonad.Layout.LayoutCombinators (JumpToLayout(..), (|||)) -- jump to layout
import XMonad.Config.Desktop
import System.Exit
import qualified XMonad.StackSet as W

-- data
import Data.Char (isSpace)
import Data.List
import Data.Maybe (fromJust)
import Data.Monoid
import Data.Maybe (isJust)
import Data.Ratio ((%)) -- for video
import qualified Data.Map as M

-- system
import System.IO (hPutStrLn) -- for xmobar

-- util
import XMonad.Util.Run (safeSpawn, unsafeSpawn, runInTerm, spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig (additionalKeysP, additionalMouseBindings)
import XMonad.Util.NamedScratchpad
import XMonad.Util.NamedWindows
import XMonad.Util.WorkspaceCompare
import XMonad.Util.Cursor

-- hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks (avoidStruts, docksStartupHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.EwmhDesktops -- to show workspaces in application switchers
import XMonad.Hooks.ManageHelpers (isFullscreen, isDialog,  doFullFloat, doCenterFloat, doRectFloat)
import XMonad.Hooks.Place (placeHook, withGaps, smart)
import XMonad.Hooks.UrgencyHook

-- actions
import XMonad.Actions.CopyWindow -- for dwm window style tagging
import XMonad.Actions.UpdatePointer -- update mouse postion
import XMonad.Actions.MouseResize
import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicProjects
import XMonad.Actions.SpawnOn

    -- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

-- layout
import XMonad.Actions.MouseResize
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.SubLayouts
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.Simplest
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.Magnifier
import XMonad.Layout.ShowWName
import XMonad.Layout.GridVariants
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.ResizableTile
import XMonad.Layout.BinarySpacePartition
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.WindowNavigation

import Colors.Gruvbox
------------------------------------------------------------------------
------------------------------------------------------------------------
--                              Variables                             --

------------------------------------------------------------------------

myTerminal      = "st"
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True
myClickJustFocuses :: Bool
myClickJustFocuses = False
myBorderWidth   = 0
myModMask       = mod4Mask
myNormalBorderColor = color13
myFocusedBorderColor = color05
myppCurrent = color06
myppVisible = color10
myppHidden = color05
myppHiddenNoWindows = color13
myppTitle = color01
myppUrgent = color10
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset
myFont :: String
myEmojiFont :: String
myFont = "xft:JetBrains Nerd Font Mono:regular:size=12:antialias=true:hinting=true"
myEmojiFont = "xft:JoyPixels:regular:size=12"
-- colorBack = "#1d2021"
-- colorFore = "#ebdbb2"
--
-- color01 = "#1d2021"
-- color02 = "#cc241d"
-- color03 = "#98971a"
-- color04 = "#d79921"
-- color05 = "#458588"
-- color06 = "#b16286"
-- color07 = "#689d6a"
-- color08 = "#a89984"
-- color09 = "#928374"
-- color10 = "#fb4934"
-- color11 = "#b8bb26"
-- color12 = "#fabd2f"
-- color13 = "#83a598"
-- color14 = "#d3869b"
-- color15 = "#8ec07c"
-- color16 = "#ebdbb2"
--
-- colorTrayer :: String
-- colorTrayer = "--tint 0x1d2021"
--
------------------------------------------------------------------------
--                           workspaces                               --
------------------------------------------------------------------------

wsdev = "dev"
wswww = "www"
wssys = "sys"
wsvid = "vid"
wsmed = "med"
wscfg = "cfg"
wsxcfg = "xcfg"

myWorkspaces = [wsdev, wswww, wssys, wsvid, wsmed, wscfg, wsxcfg]

myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices

projects :: [Project]
projects =
  [ Project { projectName      = wsxcfg
            , projectDirectory = "~/"
            , projectStartHook = Just $ do spawn "st -e nvim ~/.xmonad/xmonad.hs"
            }

  , Project { projectName      = "browser"
            , projectDirectory = "~/download"
            , projectStartHook = Just $ do spawn "conkeror"
                                           spawn "chromium"
            }
  ]

------------------------------------------------------------------------
-- desktop notifications -- dunst package required
------------------------------------------------------------------------

data LibNotifyUrgencyHook = LibNotifyUrgencyHook deriving (Read, Show)

instance UrgencyHook LibNotifyUrgencyHook where
    urgencyHook LibNotifyUrgencyHook w = do
        name     <- getName w
        Just idx <- fmap (W.findTag w) $ gets windowset

        safeSpawn "notify-send" [show name, "workspace " ++ idx]


------------------------------------------------------------------------
------------------------------------------------------------------------
--        Key bindings. Add, modify or remove key bindings here.      --
------------------------------------------------------------------------
------------------------------------------------------------------------
myKeys = [
        -- Most useful commands!!
	("M-<Return>", spawn (myTerminal)),
        ("M-S-<Return>", namedScratchpadAction scratchpads "spterminal"),
        ("M-S-p", namedScratchpadAction scratchpads "sphtop"),
        ("M-w", spawn "firefox"),
        ("C-e", spawn "emacs"),
        ("M-S-e", spawn  "st neomutt"),

        -- dmenu utilities
        ("M-d", spawn "dmenu_run -l 20 -c"), --dmenu_run
        ("M1-d", spawn "dmenuunicode"), --dmenu_run
	("C-<Home>", spawn "dmenumount"), --dmenumount
	("C-<End>", spawn "dmenuumount"), --dmenumount

	--utilities
	("C-x c", namedScratchpadAction scratchpads "calculator"),
	("C-M1-<Delete>", spawn "slock"), --lockscreen with DWM logo 🤪
	("C-t", spawn "st arigram"), --lockscreen with DWM logo
	("M-<F1>", spawn "mbsync -a"),
	("M-r", spawn "st lfrun"),

	-- Panels
        ("M-q", kill1),

	-- Layouts
	("M-<Tab>", windows W.focusDown),
	("M1-<Tab>", moveTo Next NonEmptyWS),
	("M1-S-<Tab>", moveTo Prev NonEmptyWS),
	("M-S-<Tab>",sendMessage NextLayout),
	("M-j", windows W.focusDown),
	("M-k", windows W.focusUp),
	("M-m", windows W.focusMaster),
	-- ("M-S-n", sendMessage $ Toggle NOBORDERS),
	("M-S-j",windows W.swapDown),
	("M-S-k",windows W.swapUp),
	("M-h",sendMessage Shrink),
	("M-l",sendMessage Expand),
	("M-S-<Space>",withFocused $windows . W.sink),
	("M-f", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts), -- Toggles noborder/full

	    -- KB_GROUP Increase/decrease spacing (gaps)
        ("M-x", decWindowSpacing 4),         -- Decrease window spacing
        ("M-z", incWindowSpacing 4),         -- Increase window spacing

	-- xmobar
        ("M-b", sendMessage ToggleStruts),

	--XMonad
	("C-p q", io (exitWith ExitSuccess)),
	("C-p r", spawn "xmonad --recompile; xmonad --restart" ),

	--fc keys
	("<XF86AudioMute>", spawn "pamixer -t"),
	("<XF86AudioLowerVolume>", spawn "pamixer --allow-boost -d 2"),
	("<XF86AudioRaiseVolume>", spawn "pamixer --allow-boost -i 2"),
	("<XF86MonBrightnessUp>", spawn "xbacklight -inc 15"),
	("<XF86MonBrightnessDown>", spawn "xbacklight -dec 15"),
	("<Print>", spawn "maimpick"),
	("M-S-<Print>", spawn "maim pic-full-$(date '+%y%m%d-%H%M-%S').png")
	]

myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm .|. shiftMask, button1), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    ]
------------------------------------------------------------------------
------------------------------------------------------------------------
--                                layout                              --
------------------------------------------------------------------------

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
magnify  = renamed [Replace "magnify"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ magnifier
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           -- Mirror takes a layout and rotates it by 90 degrees.
           -- So we are applying Mirror to the ThreeCol layout.
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
           $ tabbed shrinkText myTabTheme
tallAccordion  = renamed [Replace "tallAccordion"]
           $ Accordion
wideAccordion  = renamed [Replace "wideAccordion"]
           $ Mirror Accordion

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = color05
                 , inactiveColor       = color10
                 , activeBorderColor   = color05
                 , inactiveBorderColor = colorBack
                 , activeTextColor     = colorBack
                 , inactiveTextColor   = colorBack
                 }

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:JetBrainsMono Nerd Font:bold:size=60"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }



-- The layout hook
myLayout = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     noBorders tall
                                 ||| magnify
                                 ||| noBorders monocle
                                 ||| noBorders floats
                                 ||| noBorders tabs
                                 ||| noBorders grid
                                 ||| noBorders spirals
                                 ||| noBorders threeCol
                                 ||| noBorders threeRow
                                 ||| noBorders tallAccordion
                                 ||| noBorders wideAccordion


----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
-- Window rules:                                                                --
-- doFloat makes float the window.                                              --
-- doShift sends the app to a especific workspace in the list the number + 1 .  --
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

--
myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
    [ resource  =? "desktop_window" --> doIgnore
    , isFullscreen --> doFullFloat
    , resource  =? "kdesktop"       --> doIgnore
    , className =? "firefox" --> doShift  ( myWorkspaces !! 1 )
    , className =? "mpv" --> doCenterFloat
    , className =? "nuclear" --> doShift  ( myWorkspaces !! 8 )
    ]<+> namedScratchpadManageHook scratchpads


------------------------------------------------------------------------
------------------------------------------------------------------------
--                              scratchpads                           --
------------------------------------------------------------------------
------------------------------------------------------------------------
scratchpads :: [NamedScratchpad]
scratchpads = [ NS "spterminal" spawnTerm findTerm manageTerm ,
                NS "sphtop" spawnHtop findHtop manageHtop ,
		NS "calculator" spawnCalc findCalc manageCalc
    ]
    where
        spawnTerm = myTerminal ++ " -t terminal"
	findTerm = title =? "terminal"
	manageTerm = customFloating  $ W.RationalRect l t w h
	    where
	        h = 0.5
		w = 0.7
		t = 0.65 -h
		l = 0.85 -w

        spawnHtop = myTerminal ++ " -e htop"
	findHtop = title =? "htop"
	manageHtop = customFloating  $ W.RationalRect l t w h
	    where
	        h = 0.5
		w = 0.5
		t = 0.95 -h
		l = 0.95 -w

        spawnCalc  = "qalculate-gtk"
        findCalc   = className =? "Qalculate-gtk"
        manageCalc = customFloating $ W.RationalRect l t w h
            where
                h = 0.5
                w = 0.4
                t = 0.75 -h
                l = 0.70 -w
------------------------------------------------------------------------
------------------------------------------------------------------------
myStartupHook = do

    spawn "killall trayer"  -- kill current trayer on each restart
    spawnOnce "nm-applet"
    spawn ("sleep 2 && trayer --edge top --align right --width 4 --padding 5 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 100 " ++ colorTrayer ++ " --height 19")
    spawnOnce "volumeicon"
------------------------------------------------------------------------
------------------------------------------------------------------------

-- Run xmonad with the settings you specify. No need to modify this.

main = do
    -- xmproc0 <- spawnPipe "/usr/bin/xmobar -x 0 /home/javier/.config/xmobar/xmobarrc0"
    xmproc1 <- spawnPipe "/usr/bin/xmobar -x 0 /home/javier/.config/xmobar/xmobarrc1"
    xmonad
        $ ewmh
        $ dynamicProjects projects
	def
        { manageHook = manageDocks <+> myManageHook <+> manageHook desktopConfig
        , startupHook        = myStartupHook <+> setDefaultCursor xC_left_ptr
        , layoutHook         = showWName' myShowWNameTheme $ myLayout
        , handleEventHook    = handleEventHook desktopConfig
        , workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
        , terminal           = myTerminal
        , mouseBindings      = myMouseBindings
        , modMask            = myModMask
        , normalBorderColor  = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor
        , logHook = dynamicLogWithPP $ namedScratchpadFilterOutWorkspacePP $ xmobarPP
                        { ppOutput = hPutStrLn xmproc1   -- xmobar on monitor 1
                        -- { ppOutput = \x -> hPutStrLn xmproc0 x   -- xmobar on monitor 1
                              -- >> hPutStrLn xmproc1 x

                        , ppCurrent = xmobarColor myppCurrent "" . wrap ("<box type=Bottom width=1 mb=2 color=" ++ color06 ++ ">") "</box>" -- Current workspace in xmobar
                        , ppVisible = xmobarColor color06 "" . clickable              -- Visible but not current workspace
                        -- , ppHidden = xmobarColor myppHidden "" . wrap ("<box type=Bottom width=1 mb=2 color=" ++ color06 ++ ">") "</box>" . clickable  -- Hidden workspaces in xmobar
                        , ppHidden = xmobarColor myppHidden "" . wrap "[" "]" . clickable  -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor  myppHiddenNoWindows "" . clickable       -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor  myppTitle "" . shorten 70     -- Title of active window in xmobar
                        , ppSep =  "<fc=" ++ color06 ++ "> | </fc>"                     -- Separators in xmobar
                        , ppUrgent = xmobarColor  myppUrgent "" . wrap "!" "!"  -- Urgent workspace
                        , ppExtras  = [windowCount]                           -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        } -- >> updatePointer (0.25, 0.25) (0.25, 0.25)
          }`additionalKeysP` myKeys
