module Colors.Dark where

import XMonad

colorScheme = "dark"

colorBack = "#000000"
colorFore = "#c1c1c1"


color01 = "#000000"
color02 = "#121212"
color03 = "#222222"
color04 = "#333333"
color05 = "#999999"
color06 = "#FFFFFF"
color07 = "#999999"
color08 = "#c1c1c1"
color09 = "#5f8787"
color10 = "#aaaaaa"
color11 = "#777755"
color12 = "#aa9988"
color13 = "#aaaaaa"
color14 = "#888888"
color15 = "#999999"
color16 = "#444444"


colorTrayer :: String
colorTrayer = "--tint 0x000000"
